var request = require("request");
var CryptoJS = require("crypto-js");

var Service, Characteristic;

module.exports = function(homebridge) {
    Service = homebridge.hap.Service;
    Characteristic = homebridge.hap.Characteristic;

    homebridge.registerAccessory("homebridge-doorlock", "HttpLock", LockAccessory);
}

function LockAccessory(log, config) {
    this.log = log;
    this.name = config["name"];
    this.url = config["url"];
    this.lockID = config["lock-id"];
    this.token1 = config["token1"];
    this.token2 = config["token2"];
    this.type = config["type"];
    this.status = "close";

    if (this.type == "Garage") {
        this.lockservice = new Service.GarageDoorOpener(this.name);
    } else {
        this.lockservice = new Service.Door(this.name);
    }
    
    this.lockservice
        .getCharacteristic(Characteristic.CurrentDoorState)
        .on('get', this.getState.bind(this));

    this.lockservice
        .getCharacteristic(Characteristic.TargetDoorState)
        .on('get', this.getState.bind(this))
        .on('set', this.setState.bind(this));
}

LockAccessory.prototype = {
    getState: function(callback) {
		var log = this.log;
        var that = this;

        log("Getting current state...");
        var locked = (that.status == "close" ? true : false);
        log("Get current state: " + locked);
        callback(null, locked);
    },
    setState: function(state, callback) {
		var log = this.log;
        var that = this;
        
        var lockState = (state == Characteristic.CurrentDoorState.CLOSED) ? "close" : "open";
    
        log("Set state to %s", lockState);
    
        request.get({
            url: that.url + "/seed",
            json: true
        }, function(err, response, body) {
    
            if (!err && response.statusCode == 200) {
                log("Getting seed. Response: " + response + ", body: " + body);
                log("Getting seed. pwd: " + body.pwd);
    
                var relay = "RELAY=OFF";
                if (lockState == "open") {
                    relay = "RELAY=ON";
                }

                // Signature
                var aux = that.token1 + "!!" + body.pwd + "!!" + that.token2;
                var auth = CryptoJS.MD5(aux).toString();

                log("Auth header: " + auth + ", Relay: " + relay + ". URL: " + that.url + "/switch?" + relay);
    
                request.get({
                    url: that.url + "/switch?" + relay,
                    headers: {
                        'X-Auth': auth
                        }
                }, function(err, response, body) {
                    if (!err && response.statusCode == 200) {
                        // we succeeded, so update the "current" state as well
                        var currentState = (state == Characteristic.CurrentDoorState.CLOSED) ?
                            Characteristic.CurrentDoorState.OPEN : Characteristic.CurrentDoorState.CLOSED;
                        
                        that.status = lockState;
                        that.lockservice
                            .setCharacteristic(Characteristic.CurrentDoorState, currentState);
    
                        var json = JSON.parse(body);
    
                        callback(null); // success
    
                        var self = that;
                        setTimeout(function() {
                            that.status = "close";
                            self.lockservice
                                .setCharacteristic(Characteristic.CurrentDoorState, Characteristic.CurrentDoorState.CLOSED);
                        }, 5000);
                    } else {
                        log("Error '%s' setting lock state. Response: %s", err, body);
                        callback(err || new Error("Error setting lock state."));
                    }
                });
    
            } else {
                log("Error '%s' setting lock state. Response: %s", err, body);
                callback(err || new Error("Error setting lock state."));
            }
        })
    },

	identify: function(callback) {
		callback();
    },
    
    getServices: function() {
        return [ this.lockservice ];
    }
}